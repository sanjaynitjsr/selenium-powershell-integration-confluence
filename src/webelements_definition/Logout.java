package webelements_definition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Logout {
	
	/*
	 Web Elements related to Logout Page. 
	 If any changes in HTML happen , the changes should be made here. 
	  */
	
    By Confluence_homePage= By.linkText("Confluence");
    By Logout_Link =By.className("aui-avatar-inner");
    By Logout=By.linkText("Log Out");
    By Post_Logout=By.xpath("//button[@type='submit']");
    
    WebDriver driver = Test_Flow_and_Test_Parameters.General_Test_Parameters.getDriver("FirefoxDriver");
    
    
    public void Click_Home_Page()
    {
    	driver.findElement(Confluence_homePage).click();
    }	
    
    public void Click_Logout_Link()
    {
    	driver.findElement(Logout_Link).click();
    }
    
    
    public void Click_Post_Logout()
    {
    	driver.findElement(Post_Logout).click();
    }
    
    
    public void Click_Logout_Button()
    {
    	driver.findElement(Logout).click();
    }
    

}
