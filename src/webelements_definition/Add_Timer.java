package webelements_definition;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;


public class Add_Timer {
	/*
	 Call this class to add an implicit wait. 
	 */

	WebDriver driver = Test_Flow_and_Test_Parameters.General_Test_Parameters.getDriver("FirefoxDriver");
	public void Add_a_timer()
    {
    	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

}
