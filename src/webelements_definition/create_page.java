 
package webelements_definition;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/*
 Web Elements related to Create Page. 
 If any changes in HTML happen , the changes should be made here. 
  */

public class create_page {


	WebDriver driver = Test_Flow_and_Test_Parameters.General_Test_Parameters.getDriver("FirefoxDriver");
  
        
        By display_Spaces=By.id("space-menu-link");
        By Select_Space= By.linkText("sanjaymukherji");
        By Create_Page= By.linkText("Create");
        By Add_Title=By.id("content-title");
 //     By Submit_Page= By.xpath("//button[@type='submit']");
        By Submit_Page= By.xpath(".//*[@id='rte-button-publish']");
        
       
        public void Show_all_spaces()
        {
        	driver.findElement(display_Spaces).click();
        	
        }	
        
        public void Select_from_spaces()
        {
        	driver.findElement(Select_Space).click();
        }
        
        
        public void Create_a_Page()
        {
        	driver.findElement(Create_Page).click();
        	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        
        public void Add_a_Title(String tit )
        {
        	driver.findElement(Add_Title).sendKeys(tit);
        }
        
        public void Submit_a_Page()
        {
        	driver.findElement(Submit_Page).click();
        }
        
            
	}
      



