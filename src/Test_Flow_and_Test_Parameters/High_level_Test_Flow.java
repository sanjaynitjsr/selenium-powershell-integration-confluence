package Test_Flow_and_Test_Parameters;
import java.io.IOException;
import org.testng.annotations.Test;
import PageCreation_TestCases.Create_Page_Process;
import PageCreation_TestCases.Login_Process;
import PageCreation_TestCases.Logout_Page_Process;

public class High_level_Test_Flow {

	/*
	 Main Test Flow ,All Process are invoked from this Class. 
	 Run this To Execute!
	 */

	@Test
	public static void Calling_All_Processes() throws IOException {

	Login_Process Login_Process=new Login_Process();
	Create_Page_Process Create_Page_Process= new Create_Page_Process();
	Logout_Page_Process Logout_Page_Process= new Logout_Page_Process();
	
	Login_Process.Login_Process_Level_Validation();
	Create_Page_Process.Create_Page_Process_Level_Validation();
	Logout_Page_Process.Logout_Page_Process_Level_Validation();
	
	
	
	}
}
