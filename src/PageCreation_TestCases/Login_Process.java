package PageCreation_TestCases;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.java.performance_diagnostics.Excecute_Powershell_Script;

import Test_Classes.All_Pages;
import Test_Flow_and_Test_Parameters.General_Test_Parameters;

public class Login_Process {
	
	/*
	 Login Page Process.
	 Invokes All Pages as Part of the Create Page Process and Calculates the time of Creation 
	 and Launches Performance Diagnostics depending on the Response Time. 
	 */
	
	WebDriver driver;
    @Test
	public void Login_Process_Level_Validation() throws IOException {
	All_Pages All_pages=new All_Pages();
	Excecute_Powershell_Script ExcecuteScript= new Excecute_Powershell_Script();
	
	Date start= new Date();
	All_pages.LoginPage();
	Date end= new Date();
	
	long sla=1;
	long duration  = end.getTime()-start.getTime();	
	long Login_time = TimeUnit.MILLISECONDS.toMillis(duration);
	
	System.out.println(start);
	System.out.println(end);
	System.out.println("The Time Taken to Login"+Login_time);
	
	
	if (Login_time>sla)
	{System.out.println("This is quite high, calling diagnostics");
	
	if(General_Test_Parameters.Performance_Diagnostics_Flag_on_or_off=="on")
	{ExcecuteScript.main();}
	else{
		System.out.println("Diagnostic mode is off");}
	}else
	{System.out.println("This is NOT high not calling Diagnostics");}
	
	
	
	

	}

	
	
}
	



