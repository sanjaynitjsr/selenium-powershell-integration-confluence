The above Script Functionally Tests the Create Page Functionality also does Frontend Performance Evaluation and Host Metrics Diagnostics . Please read below for detailed description .

a) Creates multiple files, logs in once per script run . Number of files created depends on 
the number is set in Test Control Parameters . It picks up the names from a excel data source. 

b) Test Control Parameters has the following test definitions. 


a) Number of Files to be created. 


b) If the script is to be run in Performance Diagnostic mode. (Executes classes calculating the time for each process execution and calls set of Powershell scripts) 


c) Has verification based on xpaths of expected elements on every page.

e) Outputs the Total and Average response time along with number of pages created. (Number of Pages created is based on number of Iterations set in General Test Parameters.)

f) Invokes Powershell Script only if Response Time SLA’s are not met , this is calculated during the execution of the script. 

g) The Powershell Script captures System Host Metrics and runs some basic Diagnostics on what may be wrong. 


Note : The Powershell Script should be executed remotely on the Server where the Application is hosted. ( For Demonstration purposes , this executes on the local system) 



Also , 

This is an ongoing effort currently I am working on conceptulizing ways to achieve the below , which I believe would be very helpful for any client server application.
Refereces : 14 Rules for frontend optoimization by Steve Soulders. 


1)	Layer of error and run prompt accordingly: Page Object Model framework applies on the HTML elements, hence if an frontend element changes it might not be a functional failure but just a front end change. I would want to work on framework which would distinguish a functional error from a front end change. ( more than implementing try catch blocks) 

2) Make a call the below sections in case of high response times. 

•	Size of Images, number of images
•	Total bytes passed over the network
•	Check for Redirects per Page. 
•	Check the number of HTTP requests made per Page.  
•	( check overtime how dynamic  a particular component of a particular web
page is and check the frequency of download (change in expires header or 
usage of CDN can be adjusted accordingly)

3) Make a batch file of the script and schedule runs at random times during the 
day and track the following 

•	plot response time with respect to the time of the day 
•	plot response time to network speeds 
•	plot response time to number of pages created / number of pages 
•	present in the Page 
•	check the above for different browsers 
•	run some analytics on data gathered from above to check the relation with each other ( possibly automate it) 
•	Check for application availability


The below Code should be placed on C:/ drive with .ps1 extension. This is a high level diagnostics of host metrics , this is integrated with the above Selenium Project



# Total CPU utilization


 $ProcessNo= Get-Counter -Counter "\Processor(_Total)\% Processor Time" 
 $Sample= $ProcessNo.CounterSamples[0]
 $CpuTime = $Sample.CookedValue
 
 
 $ProcessNo

if ($CpuTime -gt 70 ) 
{
 "The value is greater than the threshold of 70% CPU" 
}
else
{
  "The value is less than the CPU threshold of 70% CPU" 
}



# Processor / Interrupts per sec 



 $ProcessNo= Get-Counter -Counter "\Processor(_Total)\% Processor Time" 
 $Sample= $ProcessNo.CounterSamples[0]
 $CpuTime = $Sample.CookedValue
 
  
 $ProcessNo= Get-Counter -Counter "\Processor(_total)\% Interrupt Time" 
 $Dummy= $ProcessNo.CounterSamples[0]
 $IntTime = $Dummy.CookedValue
 $CpuTime
 $IntTime

 
if ($CpuTime -gt 90 ) 
{
 "The value is greater than the threshold of 90% CPU" 
    if ($IntTime -gt 15)
        {
        "The Interrupt Time is greater than the threshold of 15% CPU" 
            }
            else
             { "The CPU time is greater that 90 % but the value of Interput time is less than the threshold of 15% CPU" 
                 }
         }
else
{
  "The CPU and Interupt time is less that the threshold limit" 
}

# System/Context Switches per sec
 $Sample1= $ProcessNo.CounterSamples[0]
 $Cswitches = $Sample1.CookedValue
 $Cswitches
 
 if ($Cswitches -gt 500)
  {
   "There is a problem with Network Adapter"
   }
   else{
         "There is no problem with the network adapter!!!"
         }
 




 








Thank you .
 
Sanjay Mukherji


0413 243 862