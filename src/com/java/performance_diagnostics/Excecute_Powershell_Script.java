package com.java.performance_diagnostics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Excecute_Powershell_Script {
	
	/*
	 Calls the Powershell Diagnostic Script. 
	 The Powershell script needs to be kept in the below Location. 
	 

	 */

	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public void main() throws IOException {
		String command = "powershell.exe  \"C:/sanjayexe1\\CPU.ps1\" ";
		Process powerShellProcess = Runtime.getRuntime().exec(command);
		powerShellProcess.getOutputStream().close();
		String line;
		System.out.println("Output:");
		BufferedReader stdout = new BufferedReader(new InputStreamReader(
				powerShellProcess.getInputStream()));
		while ((line = stdout.readLine()) != null) {
			System.out.println(line);
		}
		stdout.close();
		System.out.println("Error:");
		BufferedReader stderr = new BufferedReader(new InputStreamReader(
				powerShellProcess.getErrorStream()));
		while ((line = stderr.readLine()) != null) {
			System.out.println(line);
		}
		stderr.close();
		System.out.println("Done");
	}

}