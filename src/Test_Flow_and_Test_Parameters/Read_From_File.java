package Test_Flow_and_Test_Parameters;


import java.io.File;
//import java.io.FileOutputStream;
import java.io.IOException;

import org.testng.annotations.Test;

//import webelements_definition.create_page;
//import Test_Classes.All_Pages;

import jxl.Workbook;
import jxl.read.biff.BiffException;
@Test
public class Read_From_File extends General_Test_Parameters  {
	
public  String Read_username_from_file()

{
	/*
	 Code to pull out data from a local file . Before executing , please Add the file to the
	 below path.
	 The Names of the Files to be created are present in this .xls file.   
	 */
	
	String datasouce;
	File src=new File("C:/Users/SANJAY/Desktop/Atla_backups/Source_data.xls");
	
	Workbook wb = null;
	try {
		wb = Workbook.getWorkbook(src);
	} catch (BiffException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}
	datasouce=wb.getSheet(0).getCell(0,0).getContents();
	System.out.print(datasouce);
	return datasouce;			
}

public  String Read_Password_from_file()

{
	String datasouce;
	File src=new File("C:/Users/SANJAY/Desktop/Atla_backups/Source_data.xls");
	
	Workbook wb = null;
	try {
		wb = Workbook.getWorkbook(src);
	} catch (BiffException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}
	datasouce=wb.getSheet(0).getCell(1,0).getContents();
	System.out.print(datasouce);
	return datasouce;		
}



public  String Read_filename_fromfile()
{
	int iterations=0;
	String datasouce;
	File src=new File("C:/Users/SANJAY/Desktop/Atla_backups/Source_data.xls");
	iterations=General_Test_Parameters.Current_Iteration_number;
	Workbook wb = null;
	try {
		wb = Workbook.getWorkbook(src);
	} catch (BiffException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {	
		e1.printStackTrace();
	}
	
	datasouce=wb.getSheet(0).getCell(2,iterations).getContents();
	System.out.println("The iteration number currently is " + iterations);
	
	return datasouce;		
		
}



}
