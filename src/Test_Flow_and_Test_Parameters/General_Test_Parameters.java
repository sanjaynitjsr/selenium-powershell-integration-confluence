package Test_Flow_and_Test_Parameters;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class General_Test_Parameters {
/*
 General Configuration used across the test . The Following Parameters are set this in Class.
 
 a) Total Iteration- The number of Pages that will be created and this will be picked up from the data file. 
 b) Current Iteration - The current Iteration running , this is used to traverse to the Data File and other places.
 c) Diagnostic Mode Flag - This Flag invokes where External Performance Diagnostics( Powershell script
  on host machine should be invoked or not) 
 d) The browser definition . 
 */
	
	private static WebDriver driver = null;
	protected static int Total_Iteration_number=3;
	protected static int Current_Iteration_number=1;
	public static String Performance_Diagnostics_Flag_on_or_off="on";

	
	public static WebDriver getDriver(String browserName)
	{
		if(driver == null)
		{
			if(browserName.equals("InternetExplorer"))
			{
				driver = new InternetExplorerDriver();
			}
			else if(browserName.equals("FirefoxDriver"))
			{
				driver = new FirefoxDriver();
			}
			else if(browserName.equals("Chrome"))
			{
				driver = new ChromeDriver();
			}
			
		}		
		return driver;
	}
	

}
