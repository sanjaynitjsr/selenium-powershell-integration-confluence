package Test_Classes;
import org.openqa.selenium.WebDriver;
import webelements_definition.Add_Timer;
import webelements_definition.Login;
import webelements_definition.Logout;
import webelements_definition.create_page;
import Verification_Test_Cases.All_Verification;
import Test_Flow_and_Test_Parameters.General_Test_Parameters;
import Test_Flow_and_Test_Parameters.Read_From_File;

public class All_Pages extends General_Test_Parameters{
	
	/*
	 All the Pages to be Called are defined here. 
	 */
	
	public int Get_Iteration_number()
	{
		int numberof_iterations;
		return numberof_iterations=General_Test_Parameters.Total_Iteration_number;
	}
	
	public void LoginPage() {
		
		WebDriver driver = Test_Flow_and_Test_Parameters.General_Test_Parameters.getDriver("FirefoxDriver");
		Read_From_File Write_Test=new Read_From_File();
		
		String Username_fromfile;
		String Password_fromfile;
		
		
		driver.manage().window().maximize();
		driver.get("https://sanjaymukherji.atlassian.net/login");
		
		Login login=new Login();
	
		Username_fromfile=Write_Test.Read_username_from_file();
		Password_fromfile=Write_Test.Read_Password_from_file();
		
		
		login.typeUserName(Username_fromfile);
		login.typePassword(Password_fromfile);
		login.ClickLoginButton();
		
	}
	
	public  void Create_Page() {
		Read_From_File Write_Test=new Read_From_File();
		String Create_filename_fromfile;
	
	
		
		create_page create_page=new create_page();
		Add_Timer Add_Timer= new Add_Timer();
		Logout Logout_Page=new Logout();
		int Total_iterations;
		Total_iterations=this.Get_Iteration_number();
		All_Verification All_Verification=new All_Verification();
		
		while(General_Test_Parameters.Current_Iteration_number<Total_iterations)
		{
			Create_filename_fromfile=Write_Test.Read_filename_fromfile();
			
			create_page.Show_all_spaces();
			create_page.Select_from_spaces();
			Add_Timer.Add_a_timer();
			create_page.Create_a_Page();
			create_page.Add_a_Title(Create_filename_fromfile);
			create_page.Submit_a_Page();
			All_Verification.Verify_Create_Page_text_check();
			Logout_Page.Click_Home_Page();
			All_Verification.Verify_Home_Page_text_check();
			Add_Timer.Add_a_timer();
			General_Test_Parameters.Current_Iteration_number=General_Test_Parameters.Current_Iteration_number+1;
		
		}
		
	}

	public void LogOut() {
		
		
		Login login=new Login();
		create_page create_page=new create_page();
		Add_Timer Add_Timer= new Add_Timer();
		Logout Logout_Page=new Logout();
		All_Verification All_Verification=new All_Verification();
		
		
		
		Logout_Page.Click_Home_Page();
		All_Verification.Verify_Home_Page_text_check();
		Logout_Page.Click_Logout_Link();
		Logout_Page.Click_Logout_Button();
		Logout_Page.Click_Post_Logout();
		All_Verification.Verify_Logout_text_check();

		
	}
	


}


