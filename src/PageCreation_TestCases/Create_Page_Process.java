package PageCreation_TestCases;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.java.performance_diagnostics.Excecute_Powershell_Script;

import Test_Classes.All_Pages;
import Test_Flow_and_Test_Parameters.General_Test_Parameters;



public class Create_Page_Process extends General_Test_Parameters {
	/*
	 Create Page Process.
	 Invokes All Pages as Part of the Create Page Process and Calculates the time of Creation 
	 and Launches Performance Diagnostics depending on the Response Time. 
	 */
	
	WebDriver driver = Test_Flow_and_Test_Parameters.General_Test_Parameters.getDriver("FirefoxDriver");
	Excecute_Powershell_Script ExcecuteScript= new Excecute_Powershell_Script();
	
	
//	@Test
	
	public void Create_Page_Process_Level_Validation() throws IOException {
	All_Pages All_pages=new All_Pages();

	Date start= new Date();
	All_pages.Create_Page();
	Date end= new Date();
	
	long duration  =  end.getTime()- start.getTime() ;
	long create_page_process_time = TimeUnit.MILLISECONDS.toMillis(duration);
	long average_time_per_iteration=create_page_process_time/General_Test_Parameters.Total_Iteration_number;
	long sla=1;
	
	

	System.out.println(start);
	System.out.println(end);
	System.out.println("The Time taken to Create"+    create_page_process_time);
	System.out.println("The Time taken to Averagee time per iteration"+    average_time_per_iteration);
	
	
	if (create_page_process_time>sla)
	{System.out.println("This is quite high, calling diagnostics");
	
	if(General_Test_Parameters.Performance_Diagnostics_Flag_on_or_off=="on")
	{ExcecuteScript.main();}
	else{
		System.out.println("Diagnostic mode is off");}
	}else
	{System.out.println("This is NOT high not calling Diagnostics");}
}
}


