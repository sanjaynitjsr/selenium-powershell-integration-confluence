package PageCreation_TestCases;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import Test_Classes.All_Pages;
import Test_Flow_and_Test_Parameters.General_Test_Parameters;

import com.java.performance_diagnostics.Excecute_Powershell_Script;

public class Logout_Page_Process {
	
	/*
	 Logout Page Process.
	 Invokes All Pages as Part of the Create Page Process and Calculates the time of Creation 
	 and Launches Performance Diagnostics depending on the Response Time. 
	 */
	
	public void Logout_Page_Process_Level_Validation() throws IOException {

	All_Pages All_pages=new All_Pages();
	Excecute_Powershell_Script ExcecuteScript= new Excecute_Powershell_Script();

	Date start= new Date();
	All_pages.LogOut();
	Date end= new Date();
	
	long duration  = end.getTime()-start.getTime();
	long Logout_time = TimeUnit.MILLISECONDS.toMillis(duration);
	long sla=1;
	
	System.out.println(start);
	System.out.println(end);
	System.out.println("The time taken to Logout"+Logout_time);
	
	if (Logout_time>sla)
	{System.out.println("This is quite high, calling diagnostics");
	
	if(General_Test_Parameters.Performance_Diagnostics_Flag_on_or_off=="on")
	{ExcecuteScript.main();}
	else{
		System.out.println("Diagnostic mode is off");}
	}else
	{System.out.println("This is NOT high not calling Diagnostics");}
	
	
	
	
}
	
}


